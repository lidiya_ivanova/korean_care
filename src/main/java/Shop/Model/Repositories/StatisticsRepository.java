package Shop.Model.Repositories;

import Shop.Model.Entities.Statistics;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface StatisticsRepository extends PagingAndSortingRepository<Statistics, Integer> {
}
