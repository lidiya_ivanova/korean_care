package Shop.Model.Repositories;

import Shop.Model.Entities.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoriesRepository extends CrudRepository<Category, Integer> {
    List<Category> findByParent_Id(int parent_id);
    Category findByName(String name);
}

