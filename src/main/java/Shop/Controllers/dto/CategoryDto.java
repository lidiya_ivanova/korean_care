package Shop.Controllers.dto;

import lombok.Data;

@Data
public class CategoryDto {
    private String name;
    private String parentName;
}

