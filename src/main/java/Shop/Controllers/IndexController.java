package Shop.Controllers;

import Shop.Model.Entities.Product;
import Shop.Model.Repositories.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


import java.util.List;
import java.util.Random;

@Controller
public class IndexController {
    private static final int PRODUCTS_PER_PAGE = 2;

    private final ProductsRepository productsRepository;

    @Autowired
    public IndexController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @RequestMapping("/")
    public String index(Model model) {

        List<Product> products = getProducts();

        model.addAttribute("products", products);

        return "index";
    }

    private List<Product> getProducts() {
        long productsCount = productsRepository.count();
        int maxPageNum = (int) (productsCount / PRODUCTS_PER_PAGE);

        Random random = new Random();
        int pageNum = random.nextInt(maxPageNum);

        Pageable pageable = PageRequest.of(pageNum,
                PRODUCTS_PER_PAGE);

        return productsRepository.findAll(pageable).getContent();
    }

}

